import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    shopCart: [],
  },
  mutations: {
    updateShopCart: function(state, product) {
      state.shopCart.push(product);
    },
  },
  actions: {
    addProductToShopCart: function(context, product) {
      context.commit('updateShopCart', product);
    },
  }
})
