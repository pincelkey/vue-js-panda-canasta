<?php

use Illuminate\Database\Eloquent\Model;

class Example extends Model {
    protected $table    = 'wp_examples';
    protected $fillable = [
        'name',
    ];
}
